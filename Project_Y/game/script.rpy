init:
    # define variable
    define y = 0
    define n = 0
    # define Character
    define Yuri = Character("Yuri", color="#0000ff", what_color="#0000ff")
    define Natsuki = Character("Natsuki", color="#ff00ff", what_color="#ff00ff")
    # define images
    image bg = "images/fon2.png"
    image bg2 = "images/fon.png"
    image Natsuki = "images/Natsuki first.png"
    image Yuri = "images/Yuri first.png"
    image Yuri_2 = "images/Yuri 2.png"
    image War = "images/War.png"
# The game starts here.
label start:
    "???" "надто чорний екран правда?)"
    "???" "давайте поставимо фон"
    scene bg with dissolve
    "???" "а тепер зміна фона"
    scene black with fade
    "ну і музичку запустим)"
    play music "music/fon.mp3"
    scene bg2 with fade
    show Yuri at left with moveinleft
    Yuri "Привіт, мене звати Юрі"
    show Natsuki at right with zoomin
    hide Yuri with dissolve
    show Yuri_2 at left with dissolve
    Natsuki "А я Нацукі"
    Yuri "Не вмішуйся!"
    Natsuki "Це ти мішаєш!!!"
    Yuri "ЗАКРИЙ РОТ!"
    Natsuki "або що? ножичок свій витягнеш?"
    hide Yuri_2
    hide Natsuki first
    with moveoutright
    show War at center
    with dissolve
    Yuri "Просто здохни"
    hide War
    "Я почув сміх десь за дверима"
    menu:
        Yuri "Тільки Юрі правда?"
        "Правда":
            $ y+=1
            "Юрі підійшла до мене..."
        "Ніколи я не буду з маньячкою!":
            $ n+=1
        "вибігти з класу":
            jump Monika
    menu:
        "Вона витягнула ніж і зібралась добити Нацукі!"
        "я помилився я буду з тобою!" if n==1:
            $ y +=1
            "Юрі підійшла до мене і..."
        "грубо вибити ніж з її рук" if n==1:
            call natsuki from _call_natsuki
        "нехай так, всерівно Юрі для мене все" if y == 1:
            Yuri "вона тільки мішала нам, правда?"
            "Я" "правда, нам ніхто не потрібний"
    if y>=1:
        "після цих слів потемніло в очах"
        "я відключився..."
        "прийшовши в себе я побачив що знаходжусь дома! А саме головне"
        "я побачив Юрі яка мирно спала біля мене"
    elif n>=1:
        "прокинувшись я побачив Нацукі яка мирно спала біля мене \nІ ніяких натяків на рани"
    #the game end here
    return
    label natsuki:
        "я дуже невдало махнув рукою і порізався в області вен"
        "Юрі побачивши це вибігла з класу"
        "Нацукі щось кричала, плакала... а потім вибігла з класу в слід за Юрі"
        hide bg2
        scene black with fade
        "В очах потемніло..."
        return
